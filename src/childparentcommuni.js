import react, { useState } from "react"


const Parent=()=>{
// const [count,setCount]=useState(0)
    const [childcount1,setChildcount1 ]=useState(0)
    const [childcount2,setChildcount2 ]=useState(0)
    const [childcount3,setChildcount3 ]=useState(0)
    const [childcount4,setChildcount4 ]=useState(0)


    return(
        <div>
          
            <h1>parent class</h1>
            <div>child1: {childcount1}, Child2: {childcount2}, child3: {childcount3} child4: {childcount4}</div>

            <Child childname="child 1" siblings = {[childcount2,childcount3,childcount4]} updateChildCount={setChildcount1}/>
            <Child childname="child 2" siblings = {[childcount1,childcount3,childcount4]} updateChildCount={setChildcount2}/>
            <Child childname="child 3" siblings = {[childcount2,childcount1,childcount4]} updateChildCount={setChildcount3}/>
            <Child childname="child 4" siblings = {[childcount2,childcount3,childcount1]} updateChildCount={setChildcount4}/>
            {/* <div onClick={()=>setCount(count + 1)}></div> */}
        </div>
    );
};export default Parent



const Child =(props)=>{
const [count,setCount]=useState(0)

function Childcountfun  (){
    setCount(count + 1)
    props.updateChildCount(count + 1)
}

    return(
        <div>
            <p>{props.childname} count - {count} - times</p>
        <div>siblings:{props.siblings[0]}</div>
            <button onClick={Childcountfun}>click me!</button>
        </div>
    )
}
